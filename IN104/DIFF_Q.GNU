# set terminal x11
set output 'diff_Q.ps'
set xlabel ' Dager'
set ylabel ' mm/ dag'
plot 'diff_Q.num' title 'Diff. mellom beregnet og observert avrenning', 'vann_Q_obs.num' title 'Observert avrenning', 'lin_diff_Q.num' title 'Line�r sammenheng mellom disse'

