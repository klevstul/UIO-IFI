! Filnavn:      delprosjekt3_4_5.sim
! Innhold:      Hovedprogram til Delprosjekt 3, 4 og 5
!
! Forfattere:   Klevstul/Presterud/Monsen
! Gruppe:       Monsen, Klevstul, Presterud
! Sist endret:  4/11-96;

BEGIN
  EXTERNAL CLASS maaledata;
  EXTERNAL PROCEDURE beregn;

! Konstanter;

  LONG REAL AK   = 0.8,     ! dag(-1);
            BK   = 0.045,   ! dag(-1);
	    Amin = 13,      ! mm;
	    Bmin = 40,      ! mm;
	    Bmax = 80,      ! mm;
	    h_steg = 0.05,  ! Steglengde, dvs. 40 punkter/dag;
	    f_sulfat;       ! Faktor for � variere sulfatmengden; 


! Globale variabler;
  REF(maaledata) mdata;
  INTEGER Antall_Dager;  ! Antall dager det er m�ledata for;

  REF(Spline_TempNedb) spl_TP; ! Temperatur og nedb�r som splines;
  REF(Kjemisk_Modell) kjmod;   ! Kjemisk modell fra Delp. 2;

  ! Data for transport av vann og sulfat;
  LONG REAL ARRAY vann_x,sulfat_x(1:2,0:180/h_steg);  ! 1=A, 2=B;

  ! Datatabeller for � gj�re beregningene i delprosjekt 4;
  ! litt raskere. CAH og CBH er kons. av H i res. A og B,;
  ! mens J_CQH er justert kons. av H i avrenningen;
  LONG REAL ARRAY CAH,CBH,J_CQH(0:180/h_steg);

  ! Differansen mellom beregnet og observert avregning;
  ! Denne arrayen brukes i delprosjekt 5i;
  LONG REAL ARRAY diff_Q(0:180); 

! Funksjoner for vanntransport;
! ----------------------------;

  ! Disse funksjonene brukes til diffligningene i vann_f;

  ! i=1 EA, i=2 EB;
  LONG REAL PROCEDURE kalk_Eab(i,k,t);
  INTEGER i,k;
  LONG REAL t; 
  BEGIN 
    IF i=1 THEN                              ! Dette er EA;
      BEGIN                                  ! vann_x(1,k) - A;
	IF vann_x(1,k) > 1 THEN kalk_Eab := 0.2 * spl_TP.Kalk_sp_T(t) ! T - temperatur;
        ELSE kalk_Eab := 0;
      END 
    ELSE                                     ! vann_x(2,k) - B;
      BEGIN                                  ! Dette er EB;
        IF vann_x(1,k) <= 1 AND Bmin < vann_x(2,k) AND vann_x(2,k) <= Bmax THEN 
          kalk_Eab := 0.2 * spl_TP.kalk_sp_T(t)
        ELSE kalk_Eab := 0;
      END;
   END;

  ! i=1 QA, i=2 QB;
  LONG REAL PROCEDURE kalk_Qab(i,k);
  INTEGER i, k;                             ! i=1 -> reservoar A, i=2 -> reservoar B;
  BEGIN
    IF i=1 THEN kalk_Qab := AK * Max(vann_x(1,k) - Amin, 0)   ! vann_x(1,k) - verdien A;
    ELSE
    IF i=2 THEN kalk_Qab :=  BK * Max(vann_x(2,k) - Bmin, 0);
  END;

  LONG REAL PROCEDURE AKSIG(k);
  INTEGER k;
  BEGIN 
    IF vann_x(2,k) <= Bmin THEN AKSIG := 1 ELSE
    IF Bmin < vann_x(2,k) AND vann_x(2,k) <= Bmax THEN 
      AKSIG := 1 - 0.25 * ( vann_x(2,k) - Bmin) / (Bmax - Bmin) ELSE
    IF Bmax < vann_x(2,k) THEN AKSIG := 0.75;
  END;


  LONG REAL PROCEDURE QOVER(k);
    INTEGER k;
  QOVER := Max(vann_x(2,k) - Bmax,0)/h_steg;

  ! Diffligninger for � beregne vanntransporten;
  LONG REAL PROCEDURE vann_f(i,k,t,x);
    INTEGER i,k;                            ! i: bestemmer funksjonen;
    LONG REAL t;                            ! k: bestemmer kolonnen;
    LONG REAL ARRAY x;
  BEGIN 
    IF i=1 THEN                             ! Ligning 1 for transport av vann;
      vann_f := spl_TP.kalk_P(t) - kalk_Eab(1,k,t) - kalk_Qab(1,k) 
    ! kalk_Qab(1,k,vann_x,Amin,Bmin) - verdien i A;
      ELSE                                  ! kalk_Eab(1,k,t) - EA;
    IF i=2 THEN                             ! Ligning 2 for transport av vann;
      vann_f := kalk_Qab(1,k) * AKSIG(k) - kalk_Eab(2,k,t) - kalk_Qab(2,k) - QOVER(k);
  END;

  ! Beregner avrenningen;
  LONG REAL PROCEDURE Kalk_Q(k);
    INTEGER k;
    Kalk_Q:=(1-AKSIG(k))*Kalk_Qab(1,k)+Kalk_Qab(2,k)+QOVER(k);
  
  ! Observert avrenning Q, konstant funksjon;
  LONG REAL PROCEDURE Kalk_Q_obs(t);
    LONG REAL t;
  BEGIN
    REF(dag) d;

    d:-mdata.data.element_number(Entier(t)+1);
    IF d=/=NONE THEN
      Kalk_Q_Obs:=d.avrenning
    ELSE
      Kalk_Q_Obs:=0;
  END;


! Funksjoner for Sulfattransport;
! ------------------------------;

  ! Disse funksjonene brukes til diffligningene i sulfat_f;

  ! Konstant funksjon som gir m�lte CPSO4;
  LONG REAL PROCEDURE Kalk_CPSO4(t);
    LONG REAL t;
  BEGIN
    REF(dag) d;

    d:-mdata.data.element_number(Entier(t)+1);
    IF d=/=NONE THEN
      Kalk_CPSO4:=f_sulfat*d.CPSO4
    ELSE
      Kalk_CPSO4:=0;
  END;

  ! Beregner CSO4 i Res. A;
  LONG REAL PROCEDURE Kalk_CASO4(k);
    INTEGER k;
  BEGIN
    Kalk_CASO4:=sulfat_x(1,k)/vann_x(1,k);
  END;

  ! Beregner CSO4 i Res. B;
  LONG REAL PROCEDURE Kalk_CBSO4(k);
    INTEGER k;
  BEGIN
    Kalk_CBSO4:=sulfat_x(2,k)/vann_x(2,k);
  END;

  ! Diffligninger for � beregne sulfattransporten;
  LONG REAL PROCEDURE sulfat_f(i,k,t,x);
    INTEGER i,   ! Hvilke funksjon som kalles;
            k;   ! Hvilke kolonne med x som skal brukes;
    LONG REAL t;
    LONG REAL ARRAY x;
  BEGIN
    IF i=1 THEN
      sulfat_f:=spl_TP.Kalk_P(t)*Kalk_CPSO4(t)-Kalk_Qab(1,k)*Kalk_CASO4(k) ELSE
    IF i=2 THEN
      sulfat_f:=AKSIG(k)*Kalk_Qab(1,k)*Kalk_CASO4(k)-(Kalk_Qab(2,k)+
                QOVER(k))*Kalk_CBSO4(k) ELSE
      sulfat_f:=0;
  END;

  ! Beregner CSO4 i avrenningen Q;
  LONG REAL PROCEDURE Kalk_CQSO4(k);
    INTEGER k;
  BEGIN
    LONG REAL q;

    q:=Kalk_Q(k);
    IF q<>0 then
      Kalk_CQSO4:=((1-AKSIG(k))*Kalk_Qab(1,k)*Kalk_CASO4(k)+(Kalk_Qab(2,k)+QOVER(k))*
	  	  Kalk_CBSO4(k))/q
    ELSE
      q:=0;
  END;
  
! Funksjoner som brukes til � tegne grafer til Delprosjekt 3;
! ----------------------------------------------------------;

  ! Vannstand i res. A;
  LONG REAL PROCEDURE Graf_Vann_A(d);
    INTEGER d; ! Hvilke dag det skal m�les for;
  BEGIN
    LONG REAL y;
    INTEGER k;
 
    ! Tar gjennomsnittet av alle beregningene for en dag;
    y:=0;
    FOR k:=d/h_steg STEP 1 UNTIL (d+1)/h_steg do
      y:=y+vann_x(1,k);

    Graf_Vann_A:=y*h_steg;
  END;

  ! Vannstand i res. B;
  LONG REAL PROCEDURE Graf_Vann_B(d);
    INTEGER d; ! Hvilke dag det skal m�les for;
  BEGIN
    LONG REAL y;
    INTEGER k;
 
    ! Tar gjennomsnittet av alle beregningene for en dag;
    y:=0;
    FOR k:=d/h_steg STEP 1 UNTIL (d+1)/h_steg do
      y:=y+vann_x(2,k);

    Graf_Vann_B:=y*h_steg;
  END;

  ! Avrenning Q fra res. A og B;
  LONG REAL PROCEDURE Graf_Avr_Q(d);
    LONG REAL d; ! Avrenning for dag;
  BEGIN
    INTEGER k;
    LONG REAL y;
    
    ! Tar gjennomsnittet av alle beregningene for en dag;
    FOR k:=d/h_steg STEP 1 UNTIL (d+1)/h_steg DO
      y:=y+Kalk_Q(k);
    
    Graf_Avr_Q:=y*h_steg;
  END;

  ! SO4 kons. i avrenning Q;
  LONG REAL PROCEDURE Graf_CQSO4(d);
    INTEGER d;
  BEGIN
    INTEGER k;
    LONG REAL y;
    
    ! Tar gjennomsnittet av alle beregningene for en dag;
    FOR k:=d/h_steg STEP 1 UNTIL (d+1)/h_steg DO
      y:=y+Kalk_CQSO4(k);
    
    Graf_CQSO4:=y*h_steg;
  END;

! Kjemiske bergningsfunksjoner til Delprosjekt 4;
! ----------------------------------------------;

  PROCEDURE Beregn_CAH;
  BEGIN 
    INTEGER k;

    ! Setter KHCa til Res. A;
    kjmod.Sett_KHCa(10**(-2.2));

    FOR k:=0 STEP 1 UNTIL (Antall_Dager-1)/h_steg DO
      CAH(k):=kjmod.Kons_H(Kalk_CASO4(k));
  END;

  PROCEDURE Beregn_CBH;
  BEGIN 
    INTEGER k;
    
    ! Setter KHCa til Res. B;
    kjmod.Sett_KHCa(10**(-3.2));

    FOR k:=0 STEP 1 UNTIL (Antall_Dager-1)/h_steg DO
      CBH(k):=kjmod.Kons_H(Kalk_CBSO4(k));
  END;

  PROCEDURE Beregn_J_CQH;
  BEGIN
    INTEGER k;

    ! Setter nye konstanter for avrenning;
    kjmod.Sett_KAlH(10**9);
    kjmod.Sett_KH(1.2&&-11);

    FOR k:=0 STEP 1 UNTIL (Antall_Dager-1)/h_steg DO
      J_CQH(k):=kjmod.Kons_J_H(Kalk_CQSO4(k),Kalk_CQCa(k));
  END;

  LONG REAL PROCEDURE Kalk_CQH(k);
    INTEGER k;
  BEGIN 
    LONG REAL q;

    q:=Kalk_Q(k);
    IF q<>0 then
      Kalk_CQH:=((1-AKSIG(k))*Kalk_Qab(1,k)*CAH(k)+(Kalk_Qab(2,k)+QOVER(k))*
	  	CBH(k))/q
    ELSE
      q:=0;
  END;

  LONG REAL PROCEDURE Kalk_CQCa(k);
    INTEGER k;
  BEGIN 
    LONG REAL q;

    q:=Kalk_Q(k);
    IF q<>0 then
      Kalk_CQCa:=((1-AKSIG(k))*Kalk_Qab(1,k)*kjmod.Kons_Ca(CAH(k))+(Kalk_Qab(2,k)+
		 QOVER(k))*kjmod.Kons_Ca(CBH(k)))/q
    ELSE
      q:=0;
  END;

  

! Funksjoner som brukes til � tegne grafer til Delprosjekt 4;
! ----------------------------------------------------------;

  LONG REAL PROCEDURE Graf_CAH(d);
    INTEGER d; ! Hvilke dag det skal m�les for;
  BEGIN
    LONG REAL y;
    INTEGER k;
 
    ! Tar gjennomsnittet av alle beregningene for en dag;
    y:=0;
    FOR k:=d/h_steg STEP 1 UNTIL (d+1)/h_steg do
      y:=y+CAH(k);

    Graf_CAH:=y*h_steg;
  END;

  LONG REAL PROCEDURE Graf_CBH(d);
    INTEGER d; ! Hvilke dag det skal m�les for;
  BEGIN
    LONG REAL y;
    INTEGER k;
 
    ! Tar gjennomsnittet av alle beregningene for en dag;
    y:=0;
    FOR k:=d/h_steg STEP 1 UNTIL (d+1)/h_steg do
      y:=y+CBH(k);

    Graf_CBH:=y*h_steg;
  END;

  LONG REAL PROCEDURE Graf_CQCa(d);
    INTEGER d; ! Hvilke dag det skal m�les for;
  BEGIN
    LONG REAL y;
    INTEGER k;
 
    ! Tar gjennomsnittet av alle beregningene for en dag;
    y:=0;
    FOR k:=d/h_steg STEP 1 UNTIL (d+1)/h_steg do
      y:=y+Kalk_CQCa(k);

    Graf_CQCa:=y*h_steg;
  END;

  LONG REAL PROCEDURE Graf_CQH(d);
    INTEGER d; ! Hvilke dag det skal m�les for;
  BEGIN
    LONG REAL y;
    INTEGER k;
 
    ! Tar gjennomsnittet av alle beregningene for en dag;
    y:=0;
    FOR k:=d/h_steg STEP 1 UNTIL (d+1)/h_steg do
      y:=y+Kalk_CQH(k);

    Graf_CQH:=y*h_steg;
  END;

  LONG REAL PROCEDURE Graf_J_CQH(d);
    INTEGER d; ! Hvilke dag det skal m�les for;
  BEGIN
    LONG REAL y;
    INTEGER k;
 
    ! Tar gjennomsnittet av alle beregningene for en dag;
    y:=0;
    FOR k:=d/h_steg STEP 1 UNTIL (d+1)/h_steg do
      y:=y+J_CQH(k);

    Graf_J_CQH:=y*h_steg;
  END;

  LONG REAL PROCEDURE Graf_J_CQAl(d);
    INTEGER d; ! Hvilke dag det skal m�les for;
  BEGIN
    LONG REAL y;
    INTEGER k;

    ! Setter nye konstanter for avrenning;
    kjmod.Sett_KAlH(10**9);
    kjmod.Sett_KH(1.2&&-11);
 
    ! Tar gjennomsnittet av alle beregningene for en dag;
    y:=0;
    FOR k:=d/h_steg STEP 1 UNTIL (d+1)/h_steg do
      y:=y+kjmod.Kons_Al(J_CQH(k));

    Graf_J_CQAl:=y*h_steg;
  END;

  LONG REAL PROCEDURE Graf_J_CQHCO3(d);
    INTEGER d; ! Hvilke dag det skal m�les for;
  BEGIN
    LONG REAL y;
    INTEGER k;

    ! Setter nye konstanter for avrenning;
    kjmod.Sett_KAlH(10**9);
    kjmod.Sett_KH(1.2&&-11);
 
    ! Tar gjennomsnittet av alle beregningene for en dag;
    y:=0;
    FOR k:=d/h_steg STEP 1 UNTIL (d+1)/h_steg do
      y:=y+kjmod.Kons_HCO3(J_CQH(k));

    Graf_J_CQHCO3:=y*h_steg;
  END;

  ! Tegne graf over elektron�ytralitet;
  LONG REAL PROCEDURE Graf_e(k);
    LONG REAL k;
  BEGIN
    LONG REAL a,b;

    ! Setter nye konstanter for avrenning;
    kjmod.Sett_KAlH(10**9);
    kjmod.Sett_KH(1.2&&-11);

    a:=3*kjmod.Kons_Al(J_CQH(k));
    b:=kjmod.Kons_HCO3(J_CQH(k));

    ! Setter standard verdier; 
    kjmod.Sett_KH(2.5&&-10);
    kjmod.Sett_KAlH(1&&9);

    Graf_e:=J_CQH(k)+2*Kalk_CQCa(k)+a-2*Kalk_CQSO4(k)-b;
  END;

! Disse funksjonene tilh�rer delprosjekt 5;
! ----------------------------------------;

  ! Arrayer for � beregne line�r sammenheng, brukes;
  ! sammen med minste kvadraters metode;
  LONG REAL ARRAY diff_X,diff_Y(0:360);

  ! Beregner differansen mellom beregnet avrenning;
  ! og observert avrenning;
  PROCEDURE beregn_diff_Q;
  BEGIN
    INTEGER i;
      FOR i := 0 STEP 1 UNTIL( antall_dager - 1) DO 
	diff_Q(i) :=  graf_avr_Q(i) - kalk_Q_obs(i);
    END;

  ! Denne proceduren brukes til � tegne graf;
  LONG REAL PROCEDURE Graf_diff_Q(d);
    INTEGER d; ! Hvilke dag det skal m�les for;
  BEGIN
    Graf_diff_Q:=diff_Q(d);
  END;

  ! Beregner en line�r sammenheng mellom diff_Q og;
  ! observert Q;
  PROCEDURE Beregn_Lin_Diff;
  BEGIN
    INTEGER d,t;
    LONG REAL a,b;
    REF(OutFile) utfil;

    t:=-1;
    ! Legger f�rst dataene fra differansen inn i arrayene;
    FOR d:=0 STEP 1 UNTIL Antall_Dager-1 DO
      BEGIN
	t:=t+1;
	diff_X(t):=d;
	diff_Y(t):=diff_Q(d);
      END;

    ! Legger s� inn fra de observerte datene;
    FOR d:=0 STEP 1 UNTIL Antall_Dager-1 DO
      BEGIN
	t:=t+1;
	diff_X(t):=d;
	diff_Y(t):=Kalk_Q_Obs(d);
      END;

    ! Beregner line�r funksjon;
    minste_kvadraters_metode(diff_X,diff_Y,0,t,a,b);

    ! Lager en graf med den beregnede linja;
    utfil:-NEW OutFile("lin_diff_Q.num");
    utfil.Open(Blanks(40));
    utfil.OutReal(0,8,15);utfil.OutChar(' ');utfil.OutReal(a*0+b,8,15);utfil.OutImage;
    utfil.OutReal(180,8,15);utfil.OutChar(' ');utfil.OutReal(a*180+b,8,15);utfil.OutImage;
    utfil.Close;
  END;

! Hovedrutiner;
! ------------------------------;

  PROCEDURE initier_euler;
  BEGIN 
    ! Setter initialverdiene for vanntransport;
    vann_x(1,0):=Amin;
    vann_x(2,0):=Bmin;

    ! Setter initialverdiene for sulfat-transport;
    sulfat_x(1,0):=f_sulfat*(4&&-5)*vann_x(1,0);
    sulfat_x(2,0):=f_sulfat*(4&&-5)*vann_x(2,0);
  END;

  PROCEDURE Utfoer_Euler;
  BEGIN
    ! Beregner simulert vannstand i res. A og B;
    eulersys(0,Antall_Dager-1,Antall_Dager/h_steg,2,vann_x,vann_f);

    ! Beregner simulert sulfatkonsentrasjon;
    eulersys(0,Antall_Dager-1,Antall_Dager/h_steg,2,sulfat_x,sulfat_f);
  END;

  ! Procedure som tester Euler ved � regne ut massebalansen for vann og sulfat;
  PROCEDURE Massebalanse_for_Vann;
  BEGIN
    LONG REAL VS,HS,t;
    INTEGER i,n;
    
    n:=(Antall_Dager-1)/h_steg;

    ! Regner f�rst ut venstresiden;
    VS:=vann_x(1,n)-vann_x(1,0)+vann_x(2,n)-vann_x(2,0);

    ! Regner s� ut h�yresiden;
    HS:=0;
    FOR i := 0 STEP 1 UNTIL n DO 
      BEGIN 
	t :=i*h_steg; ! tiden som er g�tt ved i og h_steg er steglengden;
	HS := HS + (spl_TP.kalk_P(t) - kalk_Q(i) - kalk_Eab(1,i,t)- kalk_Eab(2,i,t))* h_steg;
      END;

    OutText("  Avvik i massebalansen for vann er: ");
    OutFix(VS-HS,8,15);
    OutImage;
  END;

  PROCEDURE Massebalanse_for_Sulfat;
  BEGIN
    LONG REAL VS,HS,t;
    INTEGER i,n;
    
    n:=(Antall_Dager-1)/h_steg;

    ! Regner f�rst ut venstresiden;
    VS:=sulfat_x(1,n)-sulfat_x(1,0)+sulfat_x(2,n)-sulfat_x(2,0);

    ! Regner s� ut h�yresiden;
    HS:=0;
    FOR i := 0 STEP 1 UNTIL n DO 
      BEGIN 
	t :=i*h_steg; ! tiden som er g�tt ved i og h_steg er steglengden;
	HS := HS + (spl_TP.kalk_P(t)*Kalk_CPSO4(t) - kalk_Q(i)*Kalk_CQSO4(i))* h_steg;
      END;

    OutText("  Avvik i massebalansen for sulfat er: ");
    OutFix(VS-HS,8,15);
    OutImage;
  END;

 ! Beregner og tegner grafene til Delprosjekt 3;
  PROCEDURE Delprosjekt3_Beregn;
  BEGIN
    ! Setter standard verdier; 
    kjmod.Sett_KH(2.5&&-10);
    kjmod.Sett_KAlH(1&&9);

    OutText("  Beregner vann og sulfat transport...");OutImage;
    ! Beregner avrenning ved hjelp av Euler;
    ! Resultatet ligger i arrayene vann_x og sulfat_x;
    Initier_Euler;
    Utfoer_Euler;
  END;

  PROCEDURE Delprosjekt3_Tegne;
  BEGIN
    OutText("  Beregner massebalanse for vann og sulfat...");OutImage;
    Massebalanse_for_vann;
    Massebalanse_for_sulfat;

    OutText("  Tegner grafer...");OutImage;
    lag_graf(0,Antall_Dager-1,1,Graf_Vann_A,"vann_A.num");
    lag_graf(0,Antall_Dager-1,1,Graf_Vann_B,"vann_B.num");
    lag_graf(0,Antall_Dager-1,1,Graf_Avr_Q,"vann_Q.num");
    lag_graf(0,Antall_Dager-1,1,Kalk_Q_obs,"vann_Q_obs.num");
    lag_graf(0,Antall_Dager-1,1,Graf_CQSO4,"SO4_Q.num");    
    OutImage;
  END;

 ! Beregner og tegner grafene til Delprosjekt 4;
  PROCEDURE Delprosjekt4_Beregn;
  BEGIN
    outtext("  Beregner CAH...");OutImage;
    Beregn_CAH;
    outtext("  Beregner CBH...");OutImage;
    Beregn_CBH;
    outtext("  Beregner justert CQH...");OutImage;
    Beregn_J_CQH;
  END;

  PROCEDURE Delprosjekt4_Tegne;
  BEGIN
    OutText("  Tegner grafer...");OutImage;
    lag_graf(0,Antall_Dager-1,1,Graf_CAH,"H_A.num");
    lag_graf(0,Antall_Dager-1,1,Graf_CBH,"H_B.num");
    lag_graf(0,Antall_Dager-1,1,Graf_CQCA,"Ca_Q.num");
    lag_graf(0,Antall_Dager-1,1,Graf_J_CQH,"j_H_Q.num");
    lag_graf(0,Antall_Dager-1,1,Graf_J_CQAl,"j_Al_Q.num");
    lag_graf(0,Antall_Dager-1,1,Graf_J_CQHCO3,"j_HCO3_Q.num");
    lag_graf(0,Antall_Dager-1,h_steg,Graf_e,"elektron.num");OutImage;
  END;

 ! Tegner grafene i delprosjekt 5i;
  PROCEDURE Delprosjekt5;
  BEGIN 
    OutText("  Beregner diff mellom obs avrenning og m�lte verdier...");OutImage;
    Beregn_diff_Q;
    OutText("  Finner line�r sammenheng mellom Diff. Q og Obs. Q...");OutImage;
    Beregn_Lin_Diff;

    OutText("  Tegner grafer...");OutImage;
    lag_graf(0,Antall_Dager-1,1,Graf_diff_Q,"diff_Q.num");

    ! Beregner n� p� nytt med halv mengde sur nedb�r;
    OutImage;
    OutText(" -Simulerer halv mengde med sur nedb�r...");OutImage;
    f_sulfat:=0.5;
    Delprosjekt3_Beregn;
    Delprosjekt4_Beregn;
    OutText("  Tegner graf...");OutImage;
    lag_graf(0,Antall_Dager-1,1,Graf_J_CQH,"j_H_Q_halv.num");

    ! Beregner n� med dobbel mengde sur nedb�r;
    OutImage;
    OutText(" -Simulerer dobbel mengde med sur nedb�r...");OutImage;
    f_sulfat:=2.0;
    Delprosjekt3_Beregn;
    Delprosjekt4_Beregn;
    OutText("  Tegner graf...");OutImage;
    lag_graf(0,Antall_Dager-1,1,Graf_J_CQH,"j_H_Q_dobbel.num");
  END;

! Hovedprogram;

  ! Faktor som multipliseres med sulfatkonsentrasjonen for �;
  ! simulere mindre/mere sur nedb�r;
  f_sulfat := 1.0;

  ! Laster inn m�ledata fra fila 'in104.dta';
  mdata :- NEW maaledata;
  mdata.les_inn_data("in104.dta");
  Antall_Dager:=mdata.data.size;

  ! Genererer spline for temperatur og nedb�r (fra Delp. 2);
  spl_TP :- NEW Spline_TempNedb(mdata.data,Antall_Dager);

  ! Initierer Kjemisk Modell;
  kjmod :- NEW Kjemisk_Modell;

  ! Utf�rer delprosjektene 3, 4 og 5;
  OutText("* Delprosjekt 3");OutImage;
  Delprosjekt3_Beregn;
  Delprosjekt3_Tegne;

  OutText("* Delprosjekt 4");OutImage;
  Delprosjekt4_Beregn;
  Delprosjekt4_Tegne;

  OutText("* Delprosjekt 5");OutImage;
  Delprosjekt5;

  OutText("---");OutImage;OutImage;
END;















