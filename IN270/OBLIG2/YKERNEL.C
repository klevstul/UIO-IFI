
/*
 *  Oblig 2 In270 1998.
 *
 *  Link over UNIX sockets.
 *  
 *  Anders Hogseth <andersho@ifi.uio.no>
 *  Frode Klevstul <frodekl@ifi.uio.no>
 *  Rein Inge Hoff <reinh@ifi.uio.no>
 */


#include "ykernel.h" /* definition of struct Session   */

/*** client side ***/

linksession* Lopen(int remotePort, char *remoteHost) 
{
  linksession *sessn;
 
  int Socket; 
  int status = 0;
  struct hostent *hostPtr = NULL;
  struct sockaddr_in serverName = { 0 };
  char buffer[256] = "";
  
  sessn = (linksession *)malloc(sizeof(linksession));

  sessn->remotePort = remotePort; 
  sessn->remoteHost = (char *)malloc(strlen(remoteHost));
  strcpy(sessn->remoteHost,remoteHost); 

  Socket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (-1 == Socket)
    {
      perror("socket()");
      exit(1);
    }

  sessn->socket = Socket;

  /* need to resolve the remote server name or IP address */
  
  hostPtr = gethostbyname(remoteHost);
  if (NULL == hostPtr)
  {
    hostPtr = gethostbyaddr(remoteHost, strlen(remoteHost), AF_INET);
    if (NULL == hostPtr)
    {
      perror("Error resolving server address ");
      exit(1);
    }
  }

  serverName.sin_family = AF_INET;
  serverName.sin_port = htons(remotePort);
  memcpy(&serverName.sin_addr, hostPtr->h_addr, hostPtr->h_length);
  
  status = connect(Socket, (struct sockaddr*)&serverName, sizeof(serverName));
  if (-1 == status)
    {
      perror("connect()");
      exit(1);
    }

  return sessn;
}

/* to aid portability :-) */

int _GetHostName(char *buffer, int length)
{
    struct utsname sysname = { 0 };
    int status = 0;

    status = uname(&sysname);
    if (-1 != status)
    {
        strncpy(buffer, sysname.nodename, length);
    }
    return (status);
}

/* server side */

int LopenEnable(int remotePort) 
{
  int Socket = 0, status;
  struct hostent *hostPtr = NULL;
  char hostname[80] = "";
  struct sockaddr_in serverName = { 0 };

  Socket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (-1 == Socket)
  {
    perror("socket()");
    exit(1);
  }
  
  status = _GetHostName(hostname, sizeof(hostname));
  if (-1 == status)
  {
    perror("_GetHostName()");
    exit(1);
  }
  
  hostPtr = gethostbyname(hostname);
  if (NULL == hostPtr)
  {
    perror("gethostbyname()");
    exit(1);
  }

  memset(&serverName, 0, sizeof(serverName));
  memcpy(&serverName.sin_addr, hostPtr->h_addr, hostPtr->h_length);
    
  serverName.sin_family = AF_INET;
  serverName.sin_port = htons(remotePort); 

  status = bind(Socket, (struct sockaddr *) &serverName, sizeof(serverName));
  if (-1 == status)
  {
    perror("bind()");
    exit(1);
  }

  /* Adress created, bound to the socket and can be used to listen */
  status = listen(Socket, 5);
  if (-1 == status)
    {
      perror("listen()");
      exit(1);
    }
  
  return Socket;
}

/*** server side ***/

linksession* LopenDone(int sock) 
{
  linksession *sessn;  
  struct sockaddr_in clientName = { 0 };
  int slaveSocket, clientLength = sizeof(clientName);

  sessn = (linksession *)malloc(sizeof(linksession));
  memset(&clientName, 0, sizeof(clientName));

  slaveSocket = accept(sock, (struct sockaddr *) &clientName, &clientLength);
  if (-1 == slaveSocket)
    {
      perror("accept()");
      exit(1);
    }
  
  sessn->socket = slaveSocket;
  return sessn;
}

void RDemux(char buf[])
{
  printf("%s", buf);
}

void Lpop(char buf[], linksession *sender, int length)
{
  int status;
  status = read(sender->socket,buf,sizeof(buf));
  printf("%d: %s", status, buf);
  
  if (-1 == status)
    {
      perror("read()");
    }
  if (sizeof(buf) == 32)
    {
      /*SendAck(sender);*/
    }
}

void Lpush(char buf[], linksession* destination)
{
  write(destination->socket, buf, strlen(buf));
}

void uswrite(int fildes, const void  *buf, size_t nbyte)
{
  if(1 * rand() % 100 > 50)
    {
      write(fildes, buf, nbyte);
    }
}

/*void SendAck(linksession *sessn)
{
  
}
*/

void Keep_On_Loking(linksession *sessn)
{
  fd_set rmask, xmask, mask;
  static struct timeval timeout = { 2 , 0 };  /* five seconds */
  char buf[256];
  int nfound, bytesread;
  
  FD_ZERO(&mask);
  FD_SET(sessn->socket, &mask);
  FD_SET(fileno(stdin) ,&mask);

  for (;;) 
    {
    rmask = mask;
    nfound = select(FD_SETSIZE, &rmask, (fd_set *)0, (fd_set *)0, &timeout);
    if (nfound < 0) 
      {
	if (errno == EINTR) 
	  {
	    printf("interrupted system call\n");
	    continue;
	  }
	/* something is very wrong! */
	perror("select");
	exit(1);
      }
    if (nfound == 0) 
      {
	/* timer expired */
	printf("Timer expired\n");
	continue;
      }
    if (FD_ISSET(fileno(stdin), &rmask)) 
      {
	/* data from keyboard */
	if (!fgets(buf, sizeof(buf), stdin))
	  {
	    if (ferror(stdin)) {
	      perror("stdin");
	      exit(1);
	    }
	    exit(0);
	  }
	if (write(sessn->socket, buf, strlen(buf)) < 0)  /* Lpush anyone ??*/
	  {
	    perror("write");
	    exit(1);
	  }
      }
    if (FD_ISSET(sessn->socket, &rmask))
      {
	/* data from the super network */
	/* if ack read new message*/
	/* else check for ack */
	bytesread = read(sessn->socket, buf, sizeof buf);
	/* if not ack wait */
	/* write again */
	buf[bytesread] = '\0';
	printf("got %d bytes: %s\n", bytesread, buf); 
      }
    } /* End forever for */
}


/* main program */

int main(int argc, char *argv[])
{
  
  linksession *sessn;
  int sock, port;
  char *host;
  char buf[256] = "";

  if (argc != 4)
    {
      fprintf(stderr, "Usage: %s <serverHost> <serverPort> <s/c>\n", argv[0]);
      exit(1);
    }
  
  port = atoi(argv[2]);
 
  if(strcmp(argv[3],"s") == 0) /* server */
    { 
      sock = LopenEnable(port); 
      sessn = LopenDone(sock);
      Keep_On_Loking(sessn);
      /* Lpop(buf, sessn, 32);*/
    }
  else /* client */
    {
      host = argv[1];
      printf("OK Computer\n");
      sessn = Lopen(port, host); 
      /*printf("OK port: %d, host: %s",sessn->remotePort,sessn->remoteHost);*/
      Keep_On_Loking(sessn); /* Lpush */
      
    }
  /* work with sessn -> Lpop and Lpush (with Stop-and-wait) */ 
  /* use select to chose network or keyboard input */

  close(sessn->socket);
  return 0;
}


