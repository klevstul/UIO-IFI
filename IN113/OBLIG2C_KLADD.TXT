  /* *****************************************************************

 Sybase 10.0 Database Description File for Deloppgave 3C
 �yvind Nesse 
 Frode Klevstul  (23.10.96)
 

****************************************************************** */


/*  Table Division for the Data Model of TimeListe_Model */



Create Table Aksjepost(
	MedEier				varchar (30)	NOT NULL,
	AvdNavn 			varchar (30)	NOT NULL,
	AntAksjer   			integer		NOT NULL)

Create Table Avdeling(
	AvdNavn				varchar (30) NOT NULL,
	er_underordnet			varchar (30) NULL)



 /*  Constraint Division for the Data Model          */
 
 /*  Uniqueness Constraints  */



Alter Table Avdeling            Add Constraint A_1 Primary Key (
	AvdNavn)

Alter Table Avdeling            Add Constraint A_2 Unique (
	er_underordnet) 

Alter Table Aksjepost		Add Constraint A_3 Primary Key (
	MedEier,
	AvdNavn)

Alter Table Aksjepost           Add Constraint A_4 Unique (
	AvdNavn) 


 /*  Referential Subset Constraints */

Alter Table Avdeling		Add Constraint A_5 Foreign Key (
	AvdNavn)
       References Aksjepost (
	AvdNavn)

Alter Table Avdeling		Add Constraint A_6 Foreign Key (
	er_underordnet)
       References Aksjepost (
	AvdNavn)




/*  Insert Triggers                                  */

Create Trigger T1 on Avdeling for Insert as if (
	select count (*)
	from Avdeling A, Inserted I
	where (I.AvdNavn = A.er_underordnet and
	      I.er_underordnet = A.AvdNavn) or
              I.er_underordnet = I.AvdNavn) > 0
	begin Raiserror 200000 "F:K�lle! �:Dust!"
	Rollback Transaction
	end 

Create Trigger T2 on Aksjepost for Insert as if (
	select count (*)
	from Inserted I
	where I.AvdNavn Not In (
			select a.AvdNavn
			from Avdeling A)
	                and 
	      I.AvdNavn not in (
	                select A.er_underordnet
	                from Avdeling A))> 0
	begin Raiserror 200001 "F:P�rre! �:Dust!"
	Rollback Transaction
	end





