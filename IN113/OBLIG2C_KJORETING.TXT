  /* *****************************************************************

   Her er kj�ringen av databasen med resultater

   filnavn: ~frodekl/in113/oblig2C_kjoreting.txt

****************************************************************** */

insert into person
values ('Nesse')


insert into person
values ('Klevstul')


insert into person
values ('Burdal')


insert into person
values ('Johnsen')


insert into person
values ('Kagge')


insert into avdelingene
values ('MatNat',null)


insert into avdelingene
values ('SV',null)


insert into avdelingene
values ('HF',null)


insert into avdelingene
values ('Geografi', 'MatNat')


insert into avdelingene
values ('Kjemi', 'MatNat')


insert into avdelingene
values ('SosOek', 'SV')


insert into aksjepost
values('Nesse','MatNat',5000)


insert into aksjepost
values('Klevstul','SV',900000)


insert into aksjepost
values('Kagge','MatNat',300000)


insert into aksjepost
values('Burdal','SV',20000)
go


insert into aksjepost
values('Johnsen','Geografi',13)
go
/* Feilmelding !!! 

Msg 20001, Level 16, State 1:
Procedure 'topp_avd', Line 11:
En aksjepost kan ikke registreres p� en avdeling
som har en overordnet avdeling.                  
****************  */


update Avdelingene
set Overordnet_Avd = 'HF'
where AvdNavn = 'SV'
go
/* Feilmelding !!!

Msg 20002, Level 16, State 1:
Procedure 'sjekk_aksjepost', Line 11:
Avdelingen har tilknyttet seg aksjepost og kan derfor ikke ha
en overordnet avdeling.
****************  */


insert into aksjepost
values('Johnsen','Fysikk',13)
go
/* Feilmelding !!! ****************  */


insert into aksjepost
values('Burdal','SV',20)
go
/* Feilmelding !!! ****************  */


update aksjepost
set AntAksjer = 20020
where Eier = 'Burdal'
go


select *
from person
order by navn
go
/*  Resultat: ********************* 

 Navn                      
 ------------------------- 
 Burdal                    
 Johnsen                   
 Kagge                     
 Klevstul                  
 Nesse                     

(5 rows affected)
 Resultat slutt *******************  */


select *
from avdelingene
go
/*  Resultat: ********************* 

 AvdNavn                   Overordnet_Avd            
 ------------------------- ------------------------- 
 Geografi                  MatNat                    
 HF                        NULL    
 Kjemi                     MatNat                    
 MatNat                    NULL                      
 SosOek                    SV                        
 SV                        NULL                      

(6 rows affected)
 Resultat slutt *******************  */


select *
from aksjepost
go
/*  Resultat: ********************* 

 Eier                      Avdeling                  AntAksjer   
 ------------------------- ------------------------- ----------- 
 Burdal                    SV                              20020 
 Kagge                     MatNat                         300000 
 Klevstul                  SV                             900000 
 Nesse                     MatNat                           5000 

(4 rows affected)
 Resultat slutt *******************  */


select distinct AK.Eier
from Aksjepost AK, Avdeling AV
where AK.AntAksjer > 5000 and
      AK.Avdeling = AK.Avdeling
order by AK.AntAksjer desc
go
/*  Resultat: ********************* 

 Eier                      
 ------------------------- 
 Klevstul                  
 Kagge                     
 Burdal                    

(3 rows affected)
 Resultat slutt *******************  */

