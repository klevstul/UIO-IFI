/* ==================================== */
/* Initialiserer meta databasen  	*/
/* ==================================== */

INSERT INTO databasetingtype
VALUES ("tabell")
go

INSERT INTO databasetingtype
VALUES ("indeks")
go

INSERT INTO databasetingtype
VALUES ("fremmednokkel")
go

INSERT INTO databasetingtype
VALUES ("kandidatnokkel")
go


INSERT INTO database_priv
VALUES ("truncate")
go

INSERT INTO database_priv
VALUES ("drop")
go

INSERT INTO privilegie
VALUES ("insert")
go
INSERT INTO privilegie
VALUES ("update")
go

INSERT INTO privilegie
VALUES ("delete")
go

INSERT INTO privilegie
VALUES ("select")
go

INSERT INTO bruker
VALUES ("frodekl","enkeltbruker")
go


INSERT INTO enkeltbruker
VALUES ("frodekl","halleluja")
go

INSERT INTO skjema
VALUES ("frodekl","frodekl")
go

INSERT INTO databaseting
VALUES ("ansatt","frodekl","tabell","frodekl")
go

INSERT INTO databaseting
VALUES ("prosjekt","frodekl","tabell","frodekl")
go

INSERT INTO databaseting
VALUES ("ans_ind","frodekl","indeks","frodekl")
go


INSERT INTO databaseting
VALUES ("pk_ansatt","frodekl","kandidatnokkel","frodekl")
go



INSERT INTO tabell
VALUES ("ansatt","frodekl")
go

INSERT INTO tabell
VALUES ("prosjekt","frodekl")
go
 
INSERT INTO tabellattributt
VALUES ("ansatt","frodekl","ans#","int")
go

INSERT INTO tabellattributt
VALUES ("ansatt","frodekl","ansattnavn","char")
go

INSERT INTO tabellattributt
VALUES ("prosjekt","frodekl","prosjekt#","int")
go

INSERT INTO tabellattributt
VALUES ("prosjekt","frodekl","avdeling","int")
go

INSERT INTO nokler
VALUES ("pk_ansatt","frodekl","ansatt")
go

INSERT INTO kandidatnokler
VALUES ("pk_ansatt","frodekl")
go

INSERT INTO nokkelattributt
VALUES ("pk_ansatt","frodekl","ansatt","ans#")
go

INSERT INTO nokkelattributt
VALUES ("pk_ansatt","frodekl","prosjekt","avdeling")
go

/* ============================================================	*/
/* Msg 20018, Level 16, State 1:				*/
/* Procedure 'nokkel_trigger', Line 18:				*/
/* En nokkel kan kun bruke attributter fra en og samme tabell	*/
/* ============================================================ */

INSERT INTO indeks
VALUES ("ans_ind","frodekl","ansatt")
go

INSERT INTO indeks_att
VALUES ("ans_ind","frodekl",1,"ansatt","ansattnavn","down")
go

INSERT INTO indeks_att
VALUES ("ans_ind","frodekl",2,"prosjekt","avdeling","up")
go

/* ============================================================	*/
/* Msg 20018, Level 16, State 1:				*/
/* Procedure 'indeks_trigger', Line 16:				*/
/* En indeks kan kun bruke attributter fra en og samme tabell	*/
/* ============================================================ */
