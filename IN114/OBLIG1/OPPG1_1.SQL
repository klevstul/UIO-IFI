/*
#**************
# OPPGAVE 1.1 *
#**************


##1 
*/

SELECT	count (*)
FROM	Timeliste
WHERE	status = 'u'
go

/*
##2

# En enkel (men ikke fullgod) utgave:
*/

SELECT		sum (varighet/60)
FROM		tll_med_varighet
WHERE		mnd_start=07	AND
		aar_start=97	
GROUP BY	mnd_start
go

/*
# Regner her med at man ikke begynner aa jobbe f�r 1. aug kl 00.00 
# og fortsetter og jobbe lengre enn kl 24.00 den 31.

# Utvidet utgave:
*/

SELECT		sum (varighet/60)
FROM		tll_med_varighet
WHERE		mnd_start=07 	AND
		dag_start < 31  
GROUP BY	mnd_start 
go

/*
# +
*/

SELECT 		sum (varighet - (time_slutt*60))
FROM		tll_med_varighet
WHERE		mnd_start=07 	AND
		dag_start = 31 	AND
		time_start < 24 AND
		time_slutt > -1
GROUP BY	mnd_start
go


/*
# Hvis det hadde v�rt mulig � plusse sammen de to resultatene 
# fra de to sp�rringene hadde vi f�tt riktig svar. Hvordan gj�r man det? 


##3
*/

SELECT 	count(*)
FROM	Timelistelinje
WHERE	time_start >= time_slutt 
go


/*
##4

# Ufullstendig utgave (samme grunn som oppg.2):
*/

SELECT	sum (varighet/60)
FROM	tll_med_varighet TMV, Timeliste T
WHERE	T.timeliste# = TMV.timeliste#	AND
	mnd_start=07			AND
	aar_start=97			AND
	status <> 'u'
go


/*
# Paa oppgave 2 og 4 staar det ikke noe om at det skal spesifiseres
# noe aar, sa jeg regner med at det skal telles timer i juli 97 
*/



